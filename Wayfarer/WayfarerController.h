#pragma once

#include <memory>

#include <TartsEngine\GameController.h>

#include "WayfarerGameState.h"

class WayfarerController :
	public GameController<WayfarerGameState>
{
public:
	WayfarerController(std::shared_ptr<WayfarerGameState> &_state);
	~WayfarerController();

	// Inherited via GameController
	virtual bool HandleInputEvent(InputEvent const & evt) override;
};

