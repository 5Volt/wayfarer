#pragma once

#include <TartsEngine\GameInstance.h>

#include "WayfarerGameState.h"
#include "WayfarerController.h"

class WayfarerInstance :
	public GameInstance<WayfarerGameState, WayfarerController>
{
public:
	WayfarerInstance();
	~WayfarerInstance();
};

