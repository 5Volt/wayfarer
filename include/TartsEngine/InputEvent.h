#pragma once

#include <GMath.h>

struct InputData {
	GMath::vec2 mousePosition;
	int input_action;

	//default constructor
	InputData()
		:mousePosition()
	{}
};
// classes for handling input
enum Controller {
	INPUT_KEYBOARD,
	INPUT_MOUSE,
	INPUT_JOYSTICK, //not implemented
	NUM_INPUT_CONTROLLERS
};

struct InputEvent {
	Controller inputController;
	int key;
	InputData data;
};