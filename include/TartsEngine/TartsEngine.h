#pragma once

#include "Tarts.h"

/***
** Static Utility functions relating to the overall engine state.
***/
class TartsEngine
{
public:

	/**
	 * Perform any initialisation of external libraries which is required
	 *
	 * @return true if initialisation was successful
	 **/
	TARTS_API
	static bool Initialise();

	/**
	* determine if the engine has been initialised
	*
	* @return true if initialisation has already been done
	**/
	TARTS_API
	static bool isInitialised();

private:
	/// whether the engine has been initialised or not
	static bool is_initialised;
};