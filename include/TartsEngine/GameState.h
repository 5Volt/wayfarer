#pragma once

#include "Tarts.h"

struct GameState
{
public:
	TARTS_API
	GameState();
	TARTS_API
	~GameState();

	bool stopped = false;
};

