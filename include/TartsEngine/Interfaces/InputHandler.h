#pragma once

class InputHandler
{
public:
	virtual bool HandleInputEvent(struct InputEvent const &evt) = 0;
};
