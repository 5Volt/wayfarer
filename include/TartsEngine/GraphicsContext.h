#pragma once

#include <memory>

#include "Tarts.h"

struct GLFWwindow;

class GraphicsContext
{
public:
	TARTS_API
	GraphicsContext();

	TARTS_API
	~GraphicsContext();
	
	TARTS_API
		void SwapBuffers();

	// this window must be owned as a raw ptr
	GLFWwindow* main_window;

};

