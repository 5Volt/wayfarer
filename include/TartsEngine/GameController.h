#pragma once

#include <memory>

#include "Tarts.h"

#include "Interfaces\InputHandler.h"

#include "GameState.h"

template<typename StateType>
class GameController : public InputHandler
{
	static_assert(std::is_base_of<GameState, StateType>::value,
		"GameInstance::StateType template param must be base class of GameState");

public:
	GameController(std::shared_ptr<StateType> &_state):state(_state) {}
	
	~GameController() {};

	std::shared_ptr<StateType> state;
};

