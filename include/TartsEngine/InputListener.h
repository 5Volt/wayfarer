#pragma once

#include <map>
#include <memory>

#include "Tarts.h"

#include "InputEvent.h"

class InputHandler;

/**
* Object which handles standard GLFW HID callbacks
* All callbacks are directed to an instance of inputhandler
**/
class InputListener
{
public:
	TARTS_API
		static void StartListening(GLFWwindow *window, std::shared_ptr<InputHandler> hanlder);
private:
		static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
		static void mouse_position_callback(GLFWwindow* window, double xPos, double yPos);
		static std::shared_ptr<InputHandler> get_handler(GLFWwindow * window);

		static std::map <GLFWwindow *, std::weak_ptr<InputHandler> > windowHandlers;
		static std::map <GLFWwindow *, GMath::vec2 > mousePosition;
};

