#ifndef GMATH2D_H_INCLUDED
#define GMATH2D_H_INCLUDED
//TODO document
/**
 * This file declares and defines some common and useful functions to operator on
 * 2d standard or homogenous coordinates i.e. predominantly vec2 and mat3.
 * Ideally this file will be included instead of the GMath-Core library when required.
**/

//include the core library
#include"GMath.h"
namespace GMath {
//vector properties
template<typename T>
T azimuth(const vec<2,T> &v) {
    return -atan2(v.x(),v.y());
}

//matrix properties
template<typename T>
vec<2, T> getTranslation(const mat<3, T> &matrix) {
	return vec<2, T>(matrix[2][0], matrix[2][1]);
}

template<typename T>
vec<2, T> getScale(const mat<3, T> &matrix) {
	return vec<2, T>(matrix[0][0], matrix[1][1]);
}

//matrix operations
template<typename fromT,typename T>
void translate(mat<3,fromT> &matrix, T transX, T transY) {
    //don't need to use transformation here
    matrix[2][0] += transX;
    matrix[2][1] += transY;
}
//overload for vector
template<typename T>
inline void translate(mat<3,T> &matrix,vec<2,T> translation) {
    translate(matrix,translation.x(),translation.y());
}


template<typename fromT, typename T>
inline void scale(mat<3,fromT> &matrix, T scaleX, T scaleY) {
    //transforming is slightly innefecient, but probably not enough to warrant changing to a more intricate method
    mat<3,T> transform;
    transform[0][0] *= scaleX;
    transform[1][1] *= scaleY;
    matrix *= transform;
}

//overload for vector
template<typename fromT,typename T>
inline void scale(mat<3,fromT> &matrix, vec<2,T> scale) {
    scale(matrix, scale.x(), scale.y());
}
//overload for uniform scale
template<typename fromT,typename T>
inline void scale(mat<3,fromT> &matrix, T sF) {
    scale(matrix, sF, sF);
}

template<typename fromT, typename T>
void rotate(mat<3,fromT> &matrix, T rotation) {
    mat<3,T> transform;
    transform[0][0] = transform[1][1] = cos(rotation);
    transform[1][0] = -(transform[0][1] = sin(rotation));
    matrix *= transform;
}

template<typename fromT, typename T>
inline void rotate(vec<2, fromT> &vect, T rotation) 
{
	T sn = sin(rotation),
		cs = cos(rotation);

	vect = vec<2,T>(
		vect.x() * cs - vect.y() * sn,
		vect.x() * sn + vect.y() * cs);
}

}
#endif // GMATH2D_H_INCLUDED
