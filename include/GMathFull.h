#ifndef GMATHFULL_H_INCLUDED
#define GMATHFULL_H_INCLUDED
/**
 * this header will provide the entire GMath Core and all extension files to provide 
 * the fullest range of mathematics capabilitiesto the GMath library. 
 * Unless all extensions are going to be used,  it would be preferable to include either the core, 2d or 3d version of the library
**/
//TODO document
//core
#include"GMath.h"
//dimension specific
#include"GMath2d.h"
#include"GMath3d.h"
//extensions
#include"GMathBezier.h"


#endif // GMATHFULL_H_INCLUDED
